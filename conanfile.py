from conan import ConanFile
from qbs import Qbs
from qbsprofile import QbsProfile
from layout import qbs_layout


class ConanToolsQbs(ConanFile):
    name = "conan-tools-qbs"
    exports = "qbs.py", "qbsprofile.py", "common.py", "layout.py"
