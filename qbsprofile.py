import shlex
import shutil
import platform
import textwrap
import common

from io import StringIO
from jinja2 import Template
from conan.errors import ConanException
from conan.tools.env import VirtualBuildEnv
from conan.tools.microsoft import VCVars
from conan.tools.files import save


class LinkerFlagsParser(object):
    def __init__(self, ld_flags):
        self.driver_linker_flags = []
        self.linker_flags = []

        for item in ld_flags:
            if item.startswith('-Wl'):
                self.linker_flags.extend(item.split(',')[1:])
            else:
                self.driver_linker_flags.append(item)


class QbsProfile(object):
    filename = 'conan_toolchain_profile.qbs'
    old_filename = 'conan_toolchain.qbs'

    _optimization_map = {
        'MinSizeRel': 'small'
    }
    _cxx_language_version_map = {
        '98': 'c++98',
        'gnu98': 'c++98',
        '11': 'c++11',
        'gnu11': 'c++11',
        '14': 'c++14',
        'gnu14': 'c++14',
        '17': 'c++17',
        'gnu17': 'c++17',
        '20': 'c++20',
        'gnu20': 'c++20'
    }
    _runtime_library_map = {
        'static': 'static',
        'dynamic': 'dynamic',
        'MD': 'dynamic',
        'MT': 'static',
        'MDd': 'dynamic',
        'MTd': 'static',
    }

    _template_profile = textwrap.dedent('''\
        import qbs

        Project {
            Profile {
                name: "{{ profile }}"

                {%- for key, value in _profile_values_from_setup.items() %}
                {{ key }}: {{ value }}
                {%- endfor %}
            }
        }
        ''')

    def __init__(self, conanfile):
        self.profile = common.default_profile_name
        self._conanfile = conanfile
        self._initialized = False

    def generate(self):
        save(self, self.old_filename, self.content)
        save(self, self.filename, self.content)

    @property
    def content(self):
        if not self._initialized:
            self._lazy_init()

        context = {
            'profile': self.profile,
            '_profile_values_from_setup': self._profile_values_from_setup
        }
        t = Template(self._template_profile)
        content = t.render(**context)
        return content

    @property
    def _profiles_prefix_in_config(self):
        return 'profiles.%s' % self.profile

    def _bool(self, b):
        return None if b is None else str(b).lower()

    def _lazy_init(self):
        shutil.rmtree(common.settings_dir(self._conanfile), ignore_errors=True)
        self._check_for_compiler()
        self._build_env = self._get_build_env()
        self._setup_toolchains()

        profile_values_from_env = self._flags_from_env()
        for k, v in profile_values_from_env.items():
            self._set_qbs_config_value(k, v)

        architecture = common._architecture_map.get(
            self._conanfile.settings.get_safe('arch'))
        if architecture:
            self._set_qbs_config_value("qbs.architecture", architecture)

        build_variant = common._build_variant_map.get(
            self._conanfile.settings.get_safe('build_type'))
        if build_variant:
            self._set_qbs_config_value("qbs.buildVariant", build_variant)

        optimization = self._optimization_map.get(
            self._conanfile.settings.get_safe('build_type'))
        if optimization:
            self._set_qbs_config_value("qbs.optimization", optimization)

        cxx_language_version = self._cxx_language_version_map.get(
            str(self._conanfile.settings.get_safe('compiler.cppstd')))
        if cxx_language_version:
            self._set_qbs_config_value(
                "cpp.cxxLanguageVersion", cxx_language_version)

        target_platform = common._target_platform_map.get(
            self._conanfile.settings.get_safe('os'))
        if not target_platform:
            target_platform = "undefined"
        self._set_qbs_config_value("qbs.targetPlatform", target_platform)

        runtime_library = self._runtime_library_map.get(
            self._conanfile.settings.get_safe('compiler.runtime'))
        if runtime_library:
            self._set_qbs_config_value("cpp.runtimeLibrary", runtime_library)

        sysroot = self._build_env.get('SYSROOT')
        if sysroot:
            self._set_qbs_config_value("qbs.sysroot", sysroot)

        position_independent_code = self._bool(
            self._conanfile.options.get_safe('fPIC'))
        if position_independent_code:
            self._set_qbs_config_value("cpp.positionIndependentCode",
                                       position_independent_code)

        self._profile_values_from_setup = (
            self._read_qbs_profile_from_config())
        self._initialized = True

    def _get_build_env(self):
        virtual_build_env = VirtualBuildEnv(self._conanfile)
        return virtual_build_env.environment().vars(self._conanfile)

    def _env_var_to_list(self, var):
        return shlex.split(var)

    def _check_for_compiler(self):
        compiler = self._conanfile.settings.get_safe('compiler')
        if not compiler:
            raise ConanException('Qbs: need compiler to be set in settings')

        if compiler not in ['Visual Studio', 'gcc', 'clang']:
            raise ConanException(
                'Qbs: compiler {} not supported'.format(compiler))

    def _default_compiler_name(self):
        # needs more work since currently only windows and linux is supported
        compiler = self._conanfile.settings.get_safe('compiler')
        the_os = self._conanfile.settings.get_safe('os')
        if the_os == 'Windows':
            if compiler == 'gcc':
                arch = self._conanfile.settings.get_safe('arch')
                if arch == 'x86':
                    return 'mingw32-gcc'
                if arch == 'x86_64':
                    return 'x86_64-w64-mingw32-gcc'
            if compiler == 'Visual Studio':
                if self._conanfile.settings.get_safe("compiler.toolset") == 'ClangCL':
                    return 'clang-cl'
                return 'cl'
            if compiler == 'msvc':
                return 'cl'
            if compiler == 'clang':
                return 'clang-cl'
            raise ConanException('unknown windows compiler')

        return compiler

    def _setup_toolchains(self):
        if self._build_env.get('CC'):
            compiler = self._build_env.get('CC')
        else:
            compiler = self._default_compiler_name()

        env = None
        if platform.system() == 'Windows':
            if compiler in ['cl', 'clang-cl']:
                VCVars(self._conanfile).generate(scope=None)
                env = "conanvcvars"

        cmd = 'qbs-setup-toolchains --settings-dir "%s" %s %s' % (
            common.settings_dir(self._conanfile), compiler, self.profile)
        with self._build_env.apply():
            self._conanfile.run(cmd, env=env)

    def _read_qbs_profile_from_config(self):
        s = StringIO()
        with self._build_env.apply():
            self._conanfile.run('qbs-config --settings-dir "%s" --list' % (
                common.settings_dir(self._conanfile)), output=s)
        config = {}
        s.seek(0)
        for line in s:
            colon = line.index(':')
            if 0 < colon and not line.startswith('#'):
                full_key = line[:colon]
                if full_key.startswith(self._profiles_prefix_in_config):
                    key = full_key[len(self._profiles_prefix_in_config)+1:]
                    value = line[colon+1:].strip()
                    if value.startswith('"') and value.endswith('"'):
                        temp_value = value[1:-1]
                        if (temp_value.isnumeric() or
                                temp_value in ['true', 'false', 'undefined']):
                            value = temp_value
                    config[key] = value
        return config

    def _set_qbs_config_value(self, key, value):
        cmd = 'qbs-config --settings-dir {} profiles.{}.{} "{}"'.format(
            common.settings_dir(self._conanfile), self.profile, key, value)
        with self._build_env.apply():
            self._conanfile.run(cmd)

    def _flags_from_env(self):
        flags_from_env = {}
        if self._build_env.get('ASFLAGS'):
            flags_from_env['cpp.assemblerFlags'] = '%s' % (
                self._env_var_to_list(self._build_env.get('ASFLAGS')))
        if self._build_env.get('CFLAGS'):
            flags_from_env['cpp.cFlags'] = '%s' % (
                self._env_var_to_list(self._build_env.get('CFLAGS')))
        if self._build_env.get('CPPFLAGS'):
            flags_from_env['cpp.cppFlags'] = '%s' % (
                self._env_var_to_list(self._build_env.get('CPPFLAGS')))
        if self._build_env.get('CXXFLAGS'):
            flags_from_env['cpp.cxxFlags'] = '%s' % (
                self._env_var_to_list(self._build_env.get('CXXFLAGS')))
        if self._build_env.get('LDFLAGS'):
            parser = LinkerFlagsParser(
                self._env_var_to_list(self._build_env.get('LDFLAGS')))
            flags_from_env['cpp.linkerFlags'] = str(parser.linker_flags)
            flags_from_env['cpp.driverLinkerFlags'] = str(
                parser.driver_linker_flags)

        return flags_from_env
