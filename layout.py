import os
import common


def qbs_layout(conanfile, src_folder=None, build_folder=None):
    if src_folder:
        conanfile.folders.source = src_folder
    else:
        conanfile.folders.source = "."

    if build_folder:
        conanfile.folders.build = build_folder
    else:
        conanfile.folders.build = "build"
        if conanfile.settings.get_safe("os"):
            platform = common._target_platform_map.get(conanfile.settings.get_safe("os"))
            conanfile.folders.build = os.path.join(conanfile.folders.build, platform)
        if conanfile.settings.get_safe("arch"):
            arch = common._architecture_map.get(conanfile.settings.get_safe("arch"))
            conanfile.folders.build = os.path.join(conanfile.folders.build, arch)
        if conanfile.settings.get_safe("build_type"):
            build_type = common._build_variant_map.get(conanfile.settings.get_safe("build_type"))
            conanfile.folders.build = os.path.join(conanfile.folders.build, build_type)

    conanfile.folders.generators = os.path.join(conanfile.folders.build,
                                                "conan_generators_folder")
