default_profile_name = "conan_toolchain_profile"

def settings_dir(conanfile):
    return '%s/conan_qbs_toolchain_settings_dir' % conanfile.generators_folder

_architecture_map = {
    'x86': 'x86',
    'x86_64': 'x86_64',
    'ppc32be': 'ppc',
    'ppc32': 'ppc',
    'ppc64le': 'ppc64',
    'ppc64': 'ppc64',
    'armv4': 'arm',
    'armv4i': 'arm',
    'armv5el': 'arm',
    'armv5hf': 'arm',
    'armv6': 'arm',
    'armv7': 'arm',
    'armv7hf': 'arm',
    'armv7s': 'arm',
    'armv7k': 'arm',
    'armv8': 'arm64',
    'armv8_32': 'arm64',
    'armv8.3': 'arm64',
    'sparc': 'sparc',
    'sparcv9': 'sparc64',
    'mips': 'mips',
    'mips64': 'mips64',
    'avr': 'avr',
    's390': 's390x',
    's390x': 's390x',
    'asm.js': None,
    'wasm': None,
    'sh4le': 'sh'
}

_build_variant_map = {
    'Debug': 'debug',
    'Release': 'release',
    'RelWithDebInfo': 'profiling',
    'MinSizeRel': 'release'
}

_target_platform_map = {
    'Windows': 'windows',
    'WindowsStore': 'windows',
    'WindowsCE': 'windows',
    'Linux': 'linux',
    'Macos': 'macos',
    'Android': 'android',
    'iOS': 'ios',
    'watchOS': 'watchos',
    'tvOS': 'tvos',
    'FreeBSD': 'freebsd',
    'SunOS': 'solaris',
    'AIX': 'aix',
    'Emscripten': None,
    'Arduino': 'none',
    'Neutrino': 'qnx',
    'baremetal': 'none',
    'VxWorks': 'vxworks',
}
